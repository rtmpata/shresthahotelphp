<?php
	require_once("require_head.php");
	require_once("require_nav.php");

?>

 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
<meta property="og:image" content="img/bootstrap-social.png" />
<script src="js/jquery.js"></script>
<link rel="stylesheet" href="css/bootstrap-media-lightbox.css" />
<script src="js/bootstrap-media-lightbox.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<body>
<div class="container" style="margin-top: 80px;">
	<ol class="breadcrumb">
			<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li class="active"><span class="glyphicon glyphicon-compressed"></span> Rooms &amp; Services</li>
		</ol>

				<!-- Starts room section -->
			<div class="row">

			<!-- Article main content -->
			<article class="col-sm-8">
					<h1 class="page-title" style="background-color: #454656; color: white; text-indent: 5px; border-radius: 3px 3px 0px 0px;text-indent: 20px;" >Room</h1>
					<div class="media">
						<a href="img/Maraetaibeforesunrise.jpg" class="media-left lightbox" href="#">
							<img class="pull-left" src="img/Maraetaibeforesunrise.jpg" width="150px" style="margin-right:3px"/></a>
						<div class="media-body text-justify" style="padding-left: 5px;">
						It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
						The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
						content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum
						 as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
						 versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						</div>
						<div class="row" style="padding-top: 5px;">
							<div class="col-xs-3"> <a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="">
								<img class="img-rounded" src="img/Maraetaibeforesunrise.jpg" width="150px"/></a></div>
							<div class="col-xs-3"> <a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="">
								<img class="img-rounded" src="img/Maraetaibeforesunrise.jpg" width="150px"/></a></div>
							<div class="col-xs-3"> <a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="">
								<img class="img-rounded" src="img/Maraetaibeforesunrise.jpg" width="150px"/></a></div>
							<div class="col-xs-3"> <a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="">
								<img class="img-rounded" src="img/Maraetaibeforesunrise.jpg" width="150px"/></a></div>
						</div>





					<h1 class="page-title" style="background-color: #454656; color: white; text-indent: 5px; border-radius: 3px 3px 0px 0px;text-indent: 20px;">Services</h1>

					<!----------- free wifi  ------------>
				<div class="row">
					<div class="col-xs-3 col-sm-4" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8">
						<h4 class="text-success">Free WiFi Hotspot</h4>
						<p class="text-justify">
							its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
							It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
  						The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
  						content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum
  						 as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
  						 versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						 </p>
					</div>
				</div>

				<hr class="divider" />

					<!-------------- 24 Hours Electicity ------------>
				<div class="row">
					<div class="col-xs-3 col-sm-4" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8">
						<h4 class="text-success">24 Hours Electricity</h4>
						<p class="text-justify bg-danger">
							its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
							It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
							The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
							content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum
							 as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
							 versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
	 						The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
	 						content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum
	 						 as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various
	 						 versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
						 </p>
					</div>
				</div>

			<hr class="divider" />

			</article>
			<!-- /Article -->

			<!-- Sidebar -->
			<aside class="col-sm-4" style="border-left: 2px solid #181015; padding-top: 20px;">
				#reservation system
			</aside>
			<!-- /Sidebar -->

		</div>

	</div>
</div>
<?php
	require_once("require_foot.php");
?>
</body>
