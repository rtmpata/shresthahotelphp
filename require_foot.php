

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">

					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<b>Shrestha Hotel</b></br>
							<b>Bandipur, Tanahu, Nepal</b><br>
								<a href="#"><span class="glyphicon glyphicon-earphone"></span> <small>+977-0657364748</small></a>
								<a href="#"><span class="glyphicon glyphicon-phone"></span><small> +977-98987897</small><br /></a>
								<a href="#"><span class="glyphicon glyphicon-envelope"></span><small> brbtpmg@gmail.com</small><br /></a>
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Find Us</h3>
						<div class="widget-body">
							<p class="btn">
								<a href="" class="btn-sm btn-twitter"><i class="fa fa-twitter fa-2"></i></a>
								<a href="" class="btn-sm btn-facebook"><i class="fa fa-facebook fa-2"></i></a>
            					<a href="" class="btn-sm btn-google-plus"><i class="fa fa-google-plus"></i></a>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Bandipur</h3>
						<div class="widget-body">
							<p>Bandipur is heavenly</p>
							<p>Shrestha Hotel is here in your service..</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
					<?php	if(isset($_GET['page'])) { $n = $_GET['page'];}
							else { $n=0; }
		             ?>
								<a href="index.php?page=0"><?php if($n==0){ echo '<b style="color:white;">Home</b>'; } else{echo 'Home';}?></a> |
								<a href="service.php?page=1"><?php if($n==1){ echo '<b style="color:white;">Room &amp; Services</b>'; } else{echo 'Room &amp; Services';}?></a> |
								<a href="dining.php?page=2"><?php if($n==2){ echo '<b style="color:white;">Dinning</b>'; } else{echo 'Dinning';}?></a> |
								<a href="gallery.php?page=3"><?php if($n==3){ echo '<b style="color:white;">Gallery</b>'; } else{echo 'Gallery';}?></a> |
								<a href="about.php?page=4"><?php if($n==4){ echo '<b style="color:white;">About</b>'; } else{echo 'About';}?></a>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								<h6 class="text-center">
							Copyright &copy; 2014, Shrestha Hotel Boandipur.
							<small>Design &amp; Developed by
								 <a href="" rel="Developer">Ram Magar</a>;
							</small>
							</h6>
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</footer>





	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
