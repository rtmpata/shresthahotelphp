<?php
	require_once("require_head.php");
	require_once("require_nav.php");

?>

 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
<meta property="og:image" content="img/bootstrap-social.png" />
<body>
<div class="container" style="margin-top: 80px;">
	<ol class="breadcrumb">
			<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li class="active"><span class="glyphicon glyphicon-info-sign"></span>  About</li>
		</ol>

		<div class="row">

			<!-- Article main content -->
			<article class="col-sm-8">
				<h3 class="text-success">Shrestha Hotel</h3>
				<div class="text-justified">
				<img src="img/camera 288.jpg" alt="" class="imgithumb pull-left" width="300" style="padding-right: 5px;"/>
				<h4 class="text-success"><strong>Come home bandipur</strong></h4>
				<p class="text-justify">its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
					 and scrumptious pastries, without missing all the
					action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outsideits f
					eel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
					 and scrumptious pastries, without missing all the
					action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outside</p>
				</div>
				<div class="address">
					<b>Shrestha Hotel</b>
					<p><b>Bandipur, Tanahu, Nepal</b><br>
						<a href="#"><span class="glyphicon glyphicon-earphone"></span></a> <small>+977-0657364748</small>
						<a href="#"><span class="glyphicon glyphicon-phone"></span></a><small> +977-98987897</small><br />
						<a href="#"><span class="glyphicon glyphicon-envelope"></span></a><small> jkakfhjafjkljaf@gmail.com</small><br />
						<a class="btn btn-sm btn-social btn-facebook">
            				<i class="fa fa-facebook"></i>Follow us in Facebook</a>
            			<a class="btn btn-sm btn-social btn-google-plus">
            				<i class="fa fa-google-plus"></i>Join us in Google+</a>
					</p>
				</div>

			</article>
			<!-- /Article -->

			<!-- Sidebar -->
			<aside class="col-sm-4">

				<div class="widget">
					<h4><span class="glyphicon glyphicon-globe"></span> Find more</h4>
					<ul class="list-unstyled list-spaces" style="padding-left: 10px;">
						<li><a href=""><span class="glyphicon glyphicon-link"></span>K garne Cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Bandipur cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Gurung Dai ko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>HUriko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span> Aasutosh ko hotel</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
					</ul>
				</div>

			</aside>
			<!-- /Sidebar -->

		</div>

</div>

<!--- google map -->
<h3 class="text-success" style="padding-left: 10px;"> Our Location</h3>
	<div class="google-maps">
    	<iframe
			src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3524.8658120364776!2d84.40456062227605!3d27.93675869825495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399504f1adc07b69%3A0xb91176b94ef4c9b8!2sShrestha+Hotel+and+Lodge!5e0!3m2!1sen!2snp!4v1470296575842"
			width="600" height="450" frameborder="0" style="border:0"></iframe>
    </div>
<?php
	require_once("require_foot.php");
?>
</body>
