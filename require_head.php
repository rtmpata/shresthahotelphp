<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="brbtpmg@hotmail.com">
    <link rel="icon" href="img/logo.png"/>
    <title>SHRESTHA HOTEL, Bandipur, Tanahu, Nepal.</title>

    <!-- fonts -->
    <!--
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	-->
   	<!-- Bootstrap core CSS -->
 	 <link href="css/bootstrap-social.css" rel="stylesheet"/>
	<meta property="og:image" content="img/bootstrap-social.png" />
    <link href="css/bootstrap.css" rel="stylesheet" media="screen" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/>

    <!-- shrestha hotel css--->
    <link href="css/shotel.css" rel="stylesheet">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>

</head>
