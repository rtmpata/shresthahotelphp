
<?php
	require_once("require_head.php");
	require_once("require_nav.php");
?>

 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
 <link rel="stylesheet" type="text/css" media="all" href="daterangepicker-bs3.css" /> <!-- daterangepicker css file -->
<meta property="og:image" content="img/bootstrap-social.png" />
<!-- validation using js-->
<script src="js/jquery.validate.js"></script>
<script src="js/moment.min.js"></script> <!-- daterangepicker -->
<script src="js/daterangepicker.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
	$().ready(function() {
		// validate signup form on keyup and submit
		$("#validation").validate({
			rules:
				{
						name: {required: true,minlength: 6, maxlength: 50},
						email: {required: true,email: true},
						contact: {required: true, minlength: 8, maxlength: 25},
						checkin:{required: true, date: true, },
						checkout:{required: true},
						special_instruction: {maxlength: 500},


				},
			messages:
				{
					name: {
								required: "<small style='color: #FF6600;'>Please enter your Name.</small>",
								minlength:"<small style='color: #FF6600;'>Please enter your Full Name.</small>",
								maxlength: "<small style='color: #FF6600;'>It seems too long. Enter first and last names only.</small>"
							},
					email: "<small style='color:#FF6600;'>Please enter a valid email address.</small>",
					contact:{
									required:"<small style='color: #FF6600;'>How can we contact you?</small>",
									maxlength:"<small style='color: #FF6600;'>Seems, provide phone number or skype contact.</small>"
							},
					checkin :{
									required:"<small style='color: #FF6600;'>When you gonna visit us?</small>",
									date:"<small style='color: #FF6600;'>Please input date.</small>",
							},
					checkout :{
									required:"<small style='color: #FF6600;'>Date you gonna stay untill.</small>",
									date:"<small style='color: #FF6600;'>Please input date.</small>",
							},
					special_instruction: {maxlength:"<small style='color: #FF6600;'>Please make it short.</small>"},

			}
		});


});
	</script>
<style> body{padding-top: 50px;} </style>
<body>
	<div class="container-fluid">
	<div style="background-color: #454656; padding-top:5px; padding-bottom: 5px; color: white; border-radius: 0px 0px 3px 3px; text-align: center;">
		<h4>
			<span class="glyphicon glyphicon-pencil"></span> Book Now
		</h4>

	</div>
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="form col-lg-4">
						<form method="post" action="index.php" id="validation">
							<hr />
							<div class="form-group">
								<label for="name" class="text-success">Full Name:</label>
								<input class="form-control" name="name" id="name" type="text" placeholder="Full name" />
							</div>
							<div class="form-group">
								<label for="name" class="text-success">Email:</label>
								<input class="form-control" name="email" id="email" type="email" placeholder="Example: someone@example.com" />
							</div>
							<div class="form-group">
								<label for="contact" class="text-success">Contact:</label>
								<input class="form-control" name="contact" id="contact" type="text" placeholder="Please! Phone number or Skype or Facebook"/>
							</div>
							<hr>
							<div class="form-inline">
								<label for="chkindate" class="text-success">Check in date:</label>
		                        <div class="input-prepend input-group">
		                      		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
		                       		<input type="text" style="width: 200px" name="checkin" id="checkin" class="form-control" placeholder="mm/dd/yyyy" />
		                     	</div>

				             	<script type="text/javascript">
				               		$(document).ready(function() {
				                  	$('#checkin').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
				                    console.log(start.toISOString(), end.toISOString(), label);
				                 	 });
				               		});
				             	</script>
						<!--		<select>
									<?php  date_default_timezone_set("Asia/Katmandu"); ?>
									<option value="2015"><?php echo date('Y'); ?></option>
								</select>
								<select>
									<?php
										$m=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec');
										foreach($m as $a)
											{ ?>
												<option><?php echo $a; ?></option>
										<?php } ?>
								</select>
								<select>
									<?php
										$d=range(1, 31);
										foreach($d as $a)
											{?>
												<option><?php echo date('d'); ?></option>
										<?php } ?>
								</select>
							-->
							</div>
							<div class="form-inline">
								<label for="chkindate" class="text-success">Check in date:</label>
								<div class="input-prepend input-group">
		                      		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
		                       		<input type="text" style="width: 200px" name="checkout" id="checkout" class="form-control" placeholder="mm/dd/yyyy" />
		                     	</div>
				             	<script type="text/javascript">
				               		$(document).ready(function() {
				                  	$('#checkout').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
				                    console.log(start.toISOString(), end.toISOString(), label);
				                 	 });
				               		});
				             	</script>
						<!--		<select>
									<option value="2015"><?php echo date('Y'); ?></option>
								</select>
								<select>
									<?php
										$m=array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec');
										foreach($m as $a)
											{ ?>
												<option><?php echo $a; ?></option>
										<?php } ?>
								</select>
								<select>
									<?php
										$d=range(1,31);
										foreach($d as $a)
											{?>
												<option><?php echo $a; ?></option>
										<?php } ?>
								</select>
							</div>

						-->
							<div class="form-inline">
								<label for="adults" class="text-success">Number of rooms:</label>
								<select>
									<?php
										$room=array("Room 101", "Room 102", "Room 103", "Room 104","Room 201", "Room 202", "Room 203", "Room 204",);
										foreach($room as $r)
											{ ?>
												<option><?php echo $r; ?></option>
										<?php } ?>
								</select>
							</div>
							<hr>
							<div class="clearfix">
							</div>
							<div class="form-group">
         						<label for="special_instruction" class="text-success">Special Instruction:</label><br/>
								<textarea name="special_instruction" id="special_instruction" rows="4" cols="50" maxlength="300" placeholder="If you have to make special reservatioin..">
							</textarea>
								<i class="error"></i>
   							</div>


   						<hr />
   							<div class="form-group">
   								<label for="captcha" class="text-success">I am Human:</label><br />
									<div class="g-recaptcha" data-sitekey="6LfmSicTAAAAAFUAKvl8V6d3949XZ7ULOfiTbF-T"></div>
								</div>
							<div class="form-group">
								<button type="submit" value="cancel" class="btn btn-warning">Cancel Reservation</button>
								<button type="submit" value="submit" class="btn btn-primary">submit</button>
							</div>
				</form>
		</div>
			<div class="col-lg-4"></div>
	</div>
	</div>

<div>

	<?php
		require_once("require_foot.php");
	?>
</div>
</body>
