<?php require_once("connect.php"); ?>

<html>
<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<link rel="icon" href="../img/logo.png" />
		<title>Shrestha Hotel | dashboard</title> 
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
	<!-- css part -->
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/admin-styles.css" rel="stylesheet">
		<link rel="stylesheet" href="../css/font-awesome.min.css" />
	
	<!-- js part -->
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
</head>
<body>
	
<!-- header nav -->
	<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
  	<div class="container-fluid">
    	<div class="navbar-header">
      	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
     	 </button>
      	<a class="navbar-brand" href="#"><img src="../img/nav-logo1.png" /></a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown">
          <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
          <ul id="g-account-menu" class="dropdown-menu" role="menu">
            <li><a href="#">My Profile</a></li>
            <li></li>
          </ul>
        </li>
        <li><a href="#"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
      </ul>
    </div>
 	</div><!-- /container -->
	</div><!-- /header nav -->
	

	<!-- Main -->
	<div class="container-fluid">
	<div class="row">
		<div class="col-sm-2">
     	 <!-- Left column -->
      		<a href="#" class="hidden-xs"><strong><i class="glyphicon glyphicon-wrench"></i> Tools</strong></a>  
      
      		<hr class="hidden-xs">
      
      		<ul class="list-unstyled">
       		 <li class="nav-header"> 
       		 	<a href="#" data-toggle="collapse" data-target="#userMenu">
       		 		<h5>Settings <i class="glyphicon glyphicon-chevron-right"></i></h5>
          			</a>
            	<ul class="list-unstyled collapse" id="userMenu" >
               	 	<li> <a href="#"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                	<li><a href="#"><i class="glyphicon glyphicon-envelope"></i> Messages <span class="badge badge-info">4</span></a></li>
                	<li><a href="#"><i class="glyphicon glyphicon-cog"></i> Options</a></li>
                	<li class="nav-header"><a href="#" data-toggle="collapse" data-target="#scrolling"><i class="glyphicon glyphicon-comment"></i> Scrolling</a>
                		<form id="scrolling">
                			<input type="text" placeholder="scrolling message" name="scrolling"/>
                			<button class="btn btn-warning">Scroll</button>
                		</form>
                	</li>
                	<li><a href="user.php?page=1"><i class="glyphicon glyphicon-user"></i> Staff List</a></li>
                	<li><a href="#"><i class="glyphicon glyphicon-flag"></i> Transactions</a></li>
                	<li><a href="#"><i class="glyphicon glyphicon-exclamation-sign"></i> Rules</a></li>
                	<li><a href="#"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
            	</ul>
        	</li>
		 	<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu2">
          		<h5>Reports <i class="glyphicon glyphicon-chevron-right"></i></h5>
          		</a>
        
            	<ul class="list-unstyled collapse" id="menu2">
                	<li><a href="#">Information &amp; Stats</a></li>
                	<li><a href="#">Views</a></li>
                	<li><a href="#">Requests</a></li>
                	<li><a href="#">Timetable</a></li>
                	<li><a href="#">Alerts</a></li>
           		</ul>
        	</li>
        	<li class="nav-header">
        		<a href="#" data-toggle="collapse" data-target="#menu3">
          		<h5>Social Media <i class="glyphicon glyphicon-chevron-right"></i></h5>
          		</a>
        
            	<ul class="list-unstyled collapse" id="menu3">
                	<li><a href="#" class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i> Facebook</a></li>
                	<li><a href="#" class="btn btn-social btn-google-plus"><i class="fa fa-google-plus"></i> Google+</a></li>
            	</ul>
        	</li>
      </ul>
           
      <hr>  
  	 </div><!-- /col-3 -->
   <!-- /left column -->
   
   <div class="col-sm-10">
      	
      <!-- column 2 -->	
      <ul class="list-inline">
         <li><a href="#"<i class="glyphicon glyphicon-user"></i></a><b> Suraj Shrestha</b></li>
      </ul> 
      
      	<hr>
      	
		<div class="row">
           
            
          
            <!-- center left-->	
         	<div class="col-md-10" >
         		<div class="well">
			  <div class="well">Rooms Available<span class="badge pull-right">3</span>
			  </div>
			  <div class="btn-toobar well" role="toolbar" aria-label="rooms">
			  		<div class="btn-group" role="group" aria-label="First floor">
			  			<button class="btn btn-primary">Room 101</button>
			  			<button class="btn btn-success">Room 102</button>
			  			<button class="btn btn-success">Room 103</button>
			  			<button class="btn btn-primary">Room 104</button>
			  		</div>
			  		<div class="btn-group" role="group" aria-label="Second floor">
			  			<button class="btn btn-success" disabled>Room 201</button>
			  			<button class="btn btn-success">Room 202</button>
			  			<button class="btn btn-success">Room 203</button>
			  			<button class="btn btn-success">Room 204</button>
			  		</div>
			  	</div>
			  	<div class="row">
			  		<div class="col-md-4">
			  	<ul class="dropdown">
			  		<button class="btn btn-success dropdown-toggle" type="button" id="manualreserve" data-toggle="dropdown" aria-expanded="true">
			  			Manual Reservation <span class="caret"></span>
			  		</button>
			  		<ul class="dropdown-menu" role="menu" aria-labelledby="manualreserve">
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 101</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 102</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 103</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 104</a></li>
			  			<li class="divider"></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 201</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 202</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 203</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 204</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
			  		</ul>
			  	</ul>
			  	</div>
			  	<div class="col-md-4">
              	<ul class="dropdown">
			  		<button class="btn btn-warning dropdown-toggle" type="button" id="manualreserve" data-toggle="dropdown" aria-expanded="true">
			  			Cancel Reservation <span class="caret"></span>
			  		</button>
			  		<ul class="dropdown-menu" role="menu" aria-labelledby="manualreserve">
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 101</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 102</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 103</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 104</a></li>
			  			<li class="divider"></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 201</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 202</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 203</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 204</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
			  		</ul>
			  	</ul>
			  	</div>
			  	<div class="col-md-4">
			  		<ul class="dropdown">
			  		<button class="btn btn-default dropdown-toggle" type="button" id="manualreserve" data-toggle="dropdown" aria-expanded="true">
			  			Maintainance <span class="caret"></span>
			  		</button>
			  		<ul class="dropdown-menu" role="menu" aria-labelledby="manualreserve">
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 101</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 102</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 103</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 104</a></li>
			  			<li class="divider"></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 201</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 202</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 203</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Room 204</a></li>
			  			<li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
			  		</ul>
			  	</ul>
			  	</div>
			  	</div>
			  	</div>
              <hr>
              
              <div class="btn-group btn-group-justified">
                <a href="article.php?page=1" class="btn btn-primary col-sm-3"><i class="glyphicon glyphicon-plus"></i><br>Article</a>
                <a href="photo.php?page=1" class="btn btn-primary col-sm-3"><i class="glyphicon glyphicon-picture"></i><br>Gallery</a>
                <a href="#" class="btn btn-primary col-sm-3"><i class="glyphicon glyphicon-pencil"></i><br>Bookings</a>
                <a href="#" class="btn btn-primary col-sm-3"><i class="glyphicon glyphicon-question-sign"></i><br>About</a>
              </div>
              
              <hr>
              
           </div> <!--main -->
	 </div>
 </div></div></div>     
 
 
    <? /*
			  <!--tabs-->
              <div class="container">
                <div class="col-md-10">
                <ul class="nav nav-tabs" id="myTab">
                	<!-- TODO: modify -->
                  <li class="active"><a href="#service" data-toggle="tab"><button class="btn <?php echo "btn-primary"; ?>">Servicesss</button></a></li>
                  <li><a href="#event" data-toggle="tab"><button class="btn <?php echo "btn-default"; ?>">Events</button></a></li>
                  <li><a href="#dinning" data-toggle="tab"><button class="btn <?php echo "btn-primary"; ?>" >Dinning</button></a></li>
                </ul>
                
                <div class="tab-content">
                  <div class="tab-pane active" id="service">
                  	
                  </div>
                  <div class="tab-pane" id="event">
                  
                  </div>
                  <div class="tab-pane" id="dinning">
                   
                  </div>
                </div>
              	</div>
              </div>  
               
         */?>     <!--/tabs-->

</body>
</html>	