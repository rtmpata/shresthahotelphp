<div class="well" style="background-color: #191919;">
		<h3 class="text-center text-success" style="color: white;"><i class="glyphicon glyphicon-edit"></i> Editing Photo</h3>
	</div>

<?php
	require_once('../connect.php');

		$page=$_GET['page'];
	
	/*
	if(empty($_SESSION['user']))				//checks user privilege
		{
			$_SESSION['info']='You must login first !';
			header('location:../login.php');
		}
	 * 
	 */
	if(empty($_GET['eid']))						//checks eid from article.php where eid is 'id' from the database
		{
			header("location:../photo.php?page=$page");
		}


	$id=$_GET['eid'];
	$query="select * from tbl_gallery where id=$id";
	$q=mysql_query($query);
	$row=mysql_fetch_array($q);
	
?>
<link href="../../css/bootstrap.min.css" rel="stylesheet"/>
<script src="../../js/jquery.js"></script>
<link rel="stylesheet" href="../../css/bootstrap-media-lightbox.css" />
<script src="../../js/bootstrap-media-lightbox.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<link href="../../css/bootstrap.min.css" rel="stylesheet"/>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<body>
<div class="container">

<!-- if admin is going directly tp /action/act-article.php info is printed here -->
<div class="ifnotsubmitted" style="height: 20px; text-align: center; color: red;">
	<?php
		if(!empty($_SESSION['info']))
			{
				echo $_SESSION['info'];
			}
	?>
	
<!--javascript for addarticle is checked and returned the error messages here --> 
	<h5 id="checkonsub" style=" color: red;">		
		
	</h5>
	
</div>
		
		<form class="" action="act-photo.php?page=<?php echo $page ?>" method="post" enctype="multipart/form-data" >   <!-- passing page number of article.php in $page -->
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			
			<div class="form-group">
				<label><i class="glyphicon glyphicon-play"></i> Title</label>
				<textarea name="title" id="title"><?php echo $row['title']; ?></textarea>
			</div>
			
			
			<div>
				<label for="image"><i class="glyphicon glyphicon-picture"></i> Image:  </label>
					<a class="lightbox" title="<?php echo $row['title'];?>" href="../../img/gallery/<?php echo $row['image']; ?>" >
					<img class="img-rounded" src="../../img/gallery/<?php echo $row['image']; ?>" height="50px" /></a>
						<div style="padding-top: 3px;">
				 			<input type="file" name="image" value="<?php echo $row['image']; ?>" />
						</div>
			</div>
			<div class="form-group">
				<label for="description"><i class="glyphicon glyphicon-pencil"></i> Description</label> <br>
				<textarea class="form-control" name="description" id="art"><?php echo $row['description']; ?></textarea>
			</div>
			
			<hr>
			<div class="form-inline bg-info">
				<label for="Date"><i class="glyphicon glyphicon-th"></i> Date</label>
				<input type="date" name="date" id="date" value="<?php echo $row['date']; ?>" />
			
				<label for="category"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-th-list"></i> Category</label>
				<select class="form-control" type="text" name="category" id="category">
					<option value="<?php echo $row['category']; ?>" disabled="disabled" selected="selected"><?php echo $row['category']; ?></option>
					<option value="gallery">Gallery</option>
					<option value="slider">Slidder</option>
					<option value="room">Room</option>
				</select>
			
				<label for="Priority type"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-star"></i> Priority Type </label>
				<select name="ptype">
					<option value="<?php echo $row['ptype']; ?>" disabled="disabled" selected="selected"><?php echo $row['ptype']; ?></option>
					<option value="*****">*****</option>
					<option value="****">****</option>
					<option value="***">***</option>
					<option value="**">**</option>
					<option value="*">*</option>
				</select>
			
				<label for="status"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-eye-open"></i> Status</label>
				<select name="status">
					<option value="<?php echo $row['status']; ?>" disabled="disabled" selected="selected"><?php echo $row['status']; ?></option>
					<option value="1">1</option>
					<option value="0">0</option>
				</select>
			</div>
			<hr />
			<div class="form-inline">
				<label for="captcha"><i class="glyphicon glyphicon-lock"></i> Captcha : </label>
					<img src="../Captcha Security UseThis/CaptchaSecurityImages.php?width=150&height=35&characters=5" />
					<input type="text" name="captcha" />
					<?php
						if(isset($_SESSION['msg']))
							{
								echo "<i class='text-danger'> ***** ".$_SESSION['msg']."</i>";
								session_unset();
							}
					?>
			</div>
			<hr>
	
			<div class="row">
			<div class="col-xs-offset-4">
				<button class="btn btn-warning" type="submit" name="update" onclick="return confirm('Do you want to replace with new values !!')" >
					<i class="glyphicon glyphicon-open"></i> Update
				</button>
				<button class="btn btn-info" type="cancel" name="upcancel" value="Cancel" onclick="return confirm('Do you want to cancel update??')" >
					<i class="glyphicon glyphicon-remove"></i> Cancel
				</button>
			</div>
			</div>
			
		</form>
</div>
</body>
