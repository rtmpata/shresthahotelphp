<div class="well" style="background-color: #191919;">
	<h3 class="text-center text-success" style="color: white;"><i class="glyphicon glyphicon-plus"></i>  Add article</h3>
</div>

<?php
	require_once("../connect.php");
/*	if(empty($_SESSION['user']))
		{
			$_SESSION['info']='You must login first..';
			header('location:login.php');
		}*/
	
?>

<script src="../../js/bootstrap.min.js"></script>
<link href="../../css/bootstrap.min.css" rel="stylesheet"/>
<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
<body>
<div class="container">

		
		<form class="" action="act-article.php" method="post" enctype="multipart/form-data" >   <!-- passing page number of article.php in $page -->
			
			<div class="row">
			<div class="panel panel-default col-xs-6">
				<div class="panel-heading"><i class="glyphicon glyphicon-play"></i> Title</label></div>
				<div class="panel-body">
					<textarea name="title" id="title"></textarea>
				</div>
			</div>
			
			
			<div class="panel panel-default col-xs-6">
				<div class="panel-heading"><label for="image"><i class="glyphicon glyphicon-picture"></i> Image: </label></div>
				<div class="body">
				 	<input type="file" name="image"/>
				</div>
			</div>
			</div>
			<hr>
			<div class="form-inline bg-info">
				<label for="Date"><i class="glyphicon glyphicon-th"></i> Date</label>
				<input type="date" name="date" id="date" />
			
				<label for="category"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-th-list"></i> Category</label>
				<select class="form-control" type="text" name="category" id="category">
					<option value="service">Services</option>
					<option value="event">Events</option>
					<option value="dinning">Dinning</option>
				</select>
			
				<label for="Priority type"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-star"></i> Priority Type </label>
				<select name="ptype">
					<option value="*****">5-star</option>
					<option value="****">4-star</option>
					<option value="***">3-star</option>
					<option value="**">2-star</option>
					<option value="*">1-star</option>
				</select>
			
				<label for="status"> &nbsp; | &nbsp; <i class="glyphicon glyphicon-eye-open"></i> Status</label>
				<select name="status">
					<option value="1">1</option>
					<option value="0">0</option>
				</select>
			</div>
			<hr>
			<div class="form-group">
				<label for="description"><i class="glyphicon glyphicon-pencil"></i> Description</label> <br>
				<textarea  name="description" class="ckeditor" id="description"></textarea>
			</div>
			<div class="form-inline">
				<label for="captcha"><i class="glyphicon glyphicon-lock"></i> Captcha : </label>
					<img src="../Captcha Security UseThis/CaptchaSecurityImages.php?width=150&height=35&characters=5" />
					<input type="text" name="captcha" />
					<?php
						if(isset($_SESSION['msg']))
							{ 
								echo "<i class='text-danger'> ***** ".$_SESSION['msg']."</i>";
								session_unset();
							}
					?>
			</div>
			<hr />
			
			<hr>
			<div class="row">
			<div class="col-xs-offset-4">
				<!-- TODO: make refresh button working -->
		<!--		<a href="add-article.php">
					<button class="btn btn-info" type="button" name="" value="Cancel" onclick="return confirm('Sure to refresh?')" >
						<i class="glyphicon glyphicon-refresh"></i> Refresh
					</button>
				</a>
			-->
				<button class="btn btn-warning" type="submit" name="add">
					<i class="glyphicon glyphicon-plus"></i> Add Article
				</button>
				<button class="btn btn-info" type="cancel" name="addcancel">
					<i class="glyphicon glyphicon-remove"></i> Cancel
				</button>
			</div>
			</div>
			
		</form>
</div>
</body>
