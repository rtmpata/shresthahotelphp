<?php
	require_once('../connect.php');

		$page=$_GET['page'];
	
	/*
	if(empty($_SESSION['user']))				//checks user privilege
		{
			$_SESSION['info']='You must login first !';
			header('location:../login.php');
		}
	 * 
	 */
	if(empty($_GET['eid']))						//checks eid from article.php where eid is 'id' from the database
		{
			header("location:../user.php?page=$page");
		}


	$id=$_GET['eid'];
	$query="select * from tbl_gallery where id=$id";
	$q=mysql_query($query);
	$row=mysql_fetch_array($q);
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Shrestha Hotel Login Form</title>
		<meta name="generator" content="rtmpata@facebook.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="../../css/bootstrap.css" />
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
<body>
	<div class="well" style="background-color: #191919;">
		<h3 class="text-center text-success" style="color: white;"><i class="glyphicon glyphicon-user"></i>  Update User </h3>
	</div>
	
	<div class="container">
		<div class="row">
		<div class="col-sm-3"></div>
		<form class="col-sm-6" action="act-user.php?page=<?php echo $page; ?>" method="post" enctype="multipart/form-data" >   <!-- passing page number of article.php in $page -->
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<div class="form-group">
				<label for="username"> User Name</label>
				<span class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input type="text" name="username" class="form-control" placeholder="username" value="<?php echo $row['username']; ?>"/>
				</span>
			</div>
			<div class="form-group">
				<label for="password"> Password</label>
				<span class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" name="password" class="form-control" placeholder="<?php echo $row['password']; ?>"/>
				</span>
			</div>
			<div class="form-group">
				<label for="password"> New Password</label>
				<span class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" name="npassword" class="form-control" placeholder="New Password" />
				</span>
				<span class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" name="cpassword" class="form-control" placeholder="Confirm Password" />
				</span>
			</div>
			<div class="form-group">
				<label for="type"> Type </label>
				<select name="type">
					<option value="admin">Admin</option>
					<option value="home">Home</option>
					<option value="guest">Guest</option>
				</select>
			</div>
			
			<!--TODO: solve the dynamic input of the following problem-->
			<div class="form-group">
				<label for="Recovery Question"> Recovery Question</label>
				<br />
					<label><input type="radio" name="question" value="What is your number?" /> <?php echo $row['question'] ?></label>
				<br />
					<label><input type="radio" name="question" value="" /> <input type="text" placeholder="Your question"/></label>
				<br />
				<label for="answer">Answer</label>
				<span class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
					<input type="text" name="answer" class="form-control" placeholder="answer" value="<?php echo $row['answer']; ?>"/>
				</span>
			</div>
			<div class="form-inline">
				<label for="captcha"><i class="glyphicon glyphicon-lock"></i> Captcha : </label>
					<img src="../Captcha Security UseThis/CaptchaSecurityImages.php?width=150&height=35&characters=5" />
					<input type="text" name="captcha" />
					<?php
						if(isset($_SESSION['msg']))
							{
								echo "<i class='text-danger'> ***** ".$_SESSION['msg']."</i>";
								session_unset();
							}
					?>
			</div>
			<hr />
			<div>
		
				<button class="btn btn-warning" type="submit" name="update">
					<i class="glyphicon glyphicon-open"></i> Update
				</button>
				<button class="btn btn-info" type="cancel" name="cancel">
					<i class="glyphicon glyphicon-remove"></i> Cancel
				</button>
			</div>
			
		</form>
	
		<div class="col-sm--3"></div>
	</div> <!--row-->
</div>	<!--container-->


	<!-- script references -->
		<script src="../../js/jquery.js"></script>
		<script src="../../js/bootstrap.min.js"></script>
</body>
</html>