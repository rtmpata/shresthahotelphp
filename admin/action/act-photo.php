
<?php
    require_once('../connect.php');

		if(isset($_GET['page']))
			{
				$page=$_GET['page'];
			}




/********************* start of adding the article in the db_shrestha database containing table tbl_gallery    **********************************/

	if(isset($_POST['add']))
		{
			// captcha checkpost
			if(empty($_POST['captcha']) || $_POST['captcha']!=$_SESSION['security_code'])
				{
					$_SESSION['msg']="Please insert characters correctly !!";
					header("location:add-photo.php");
						
				}
			else
				{		
							
							 
			$title=$_POST['title'];
			
			$main=$_POST['description'];
			$date=$_POST['date'];
			$category=$_POST['category'];
			$priority=$_POST['ptype'];
			$status=$_POST['status'];

	//image upload from the form
			$image=$_FILES['image']['name'];
		if(!empty($image))
		{
			$tmp_image=$_FILES['image']['tmp_name'];
		if(move_uploaded_file($tmp_image,'../../img/gallery/'.$image))
			{
			$query="insert into tbl_gallery(title,image,description,date,category,ptype,status) values('$title','$image','$main','$date','$category','$priority','$status')";
				$q=mysql_query($query) or die();
				if($q)
					{
						$_SESSION['msg']="Photo Inserted into database successfully..";
						header("location:../photo.php?page=1");		//?page=$page redirects to article page
					}
				else 
					{
						echo 'Not inserted';
					}
			}

		else
			{	
				$query="insert into tbl_gallery(title,image,description,date,category,ptype,status) values('$title','','$main,'$date','$category','$priority','$status')";
			$q=mysql_query($query) or die();
			if($q)
				{
					$_SESSION['msg']="Article (without image) Inserted..";
					header("location:../photo.php?page=1");		//article.php?page=1 give the default page number 1.
				}
			else 
				{
					echo 'Not inserted';
				}
				
			}
		}// image compulsory end
		
	// if image is not input in form
	else
		{
			$_SESSION["image_comp"]="Image is compulsory !!";
			header("location:add-photo.php");
		}
		} // captcha end
	}
/**********************************************  adding the article end  *********************************************/




/*******  start Deleting the article from the database db_shrestha contained in table tbl_gallery ***********************/

	if(isset($_POST['addcancel'] ))
		{
			$_SESSION['msg']="Updating has been cancel..";
			header("location:../photo.php?page=$page");
		}
	
	else if(isset($_GET['did']) && $_GET['did'])
		{	
			//when Delete button on article.php is clicked
			$id=$_GET['did'];
			$query="select * from tbl_gallery where id=$id";	// selecting the image
			$q=mysql_query($query) or die();
				$row=mysql_fetch_assoc($q);
				$image=$row['image'];
			if(unlink("../../img/gallery/".$image))				// deletes the image uploaded
				{
					// when image from img/article/ is deleted
					$query="DELETE FROM tbl_gallery WHERE id=$id";	// deletes the whole article
					$q=mysql_query($query) or die();
					if($q)
						{
							// when image from folder and article from database is deleted
							$_SESSION['msg']="Photo Deleted completely !";
							header("location:../photo.php?page=$page");
						}
					else
						{
							//when image from the folder is deleted but article from database isnot deleted
							$_SESSION['msg']="Image from folder /img/gallery/ is deleted but article from database couldnot be deleted !";
							header("location:../photo.php?page=$page");
						}
				}
			else
				{
					//when image from img/gallery/ folder isnot deleted
					$query="DELETE FROM tbl_gallery WHERE id=$id";	// deletes the whole article
					$q=mysql_query($query) or die();
					$_SESSION['msg']="No image related. Article also deleted from tbl_photo";
					header("location:../photo.php?page=$page");
				}
		
		}
/**********************************************  deleting the article end  *********************************************/





/***********  start updating edited datas into the database name="update" is from edit-article.php *****************************/

	
else if(isset($_POST['upcancel'] ))
		{
			$_SESSION['msg']="Updating has been cancel..";
			header("location:../photo.php?page=$page");
		}

else if(isset($_POST['update']))
		{
			//when Update button is confirmed in edit-article.php
			$id=$_POST['id'];
			$page=$_GET['page'];
			// captcha checkpost
			if(empty($_POST['captcha']) || $_POST['captcha']!=$_SESSION['security_code'])
				{
					$_SESSION['msg']=" Please insert characters correctly !!";
					header("location:edit-photo.php?eid=$id&page=$page");
						
				} //captcha is failed end
			else
				{		
					//when captcha is passsed
			$id=$_POST['id'];
			$title=$_POST['title'];
			$description=$_POST['description'];
			$date=$_POST['date'];
			$category=$_POST['category'];
			$priority=$_POST['ptype'];
			$status=$_POST['status'];

			$current=$_FILES['image']['name'];
			if(!empty($current))
			{
				// when there is new image in <form>
				$query="select * from tbl_gallery where id=$id";
				$q=mysql_query($query) or die();
				$row=mysql_fetch_assoc($q);
				$image=$row['image'];
				if(unlink("../../img/gallery/".$image))		
					{	
						// when $current image from <form>; image in database previously too
						$tmp_image=$_FILES['image']['tmp_name'];	
						if(move_uploaded_file($tmp_image,"../../img/gallery/".$current))	
							{	
								//when old image is replaced by new $current image
								$query="update tbl_gallery set title='$title',description='$description',image='$current',date='$date',category='$category',ptype='$priority',status='$status' where id=$id";
								$q=mysql_query($query) or die();
								if($q)
									{
										// when article is updated with image replacement
										$_SESSION['msg']="Photo has been Updated..old image is replace by new one !";
										header("location:../photo.php?page=$page");
									}	
								else
									{
										//when article is not updated but image is replaced
										$_SESSION['msg']="Previous image has been deleted from folder but no other changes has been made !";
										header("location:../photo.php?page=$page");
									}
							}
					} //old image deletion end

				else		
					{
						// when $current image from <form>; but no image in database previously
						$current=$_FILES['image']['name'];
						$tmp_image=$_FILES['image']['tmp_name'];
						if(move_uploaded_file($tmp_image,"../../img/gallery/".$current))
							{
								// when new image $current is uploaded
								$query="update tbl_gallery set title='$title',description='$description',image='$current',date='$date',category='$category',ptype='$priority',status='$status' where id=$id";
								$q=mysql_query($query);
								if($q)
									{
										//when article is updated
										$_SESSION['msg']="New image is uploaded and article also updated..";
										header("location:../photo.php?page=$page");
									}
								else
									{
										//when article is not updated but new image is uploaded
										$_SESSION['msg']="New image is uploaded but article has been updated!";
										header("location:../photo.php?page=$page");
									}
							}
						else
							{
								//when no new image $current is uploaded
								$query="update tbl_gallery set title='$title',discription='$discription',image='',date='$date',category='$category',ptype='$priority',status='$status' where id=$id";
								$q=mysql_query($query);
								if($q)
									{
										//when article is updated but no new image upload
										$_SESSION['msg']="No new image is uploaded but article is updated";
										header("location:../photo.php?page=$page");
									}
								else
									{
										//when article is no updated and no new image is uploaded
										$_SESSION['msg']="No new image is uploaded and article is not updated too !!";
										header("location:../photo.php?page=$page");				
									}
							}
					}


				}


	//image is not in form
			else 	
				{
					// when image isnot uploaded currently in $current from <form>
					$query="select * from tbl_gallery where id=$id";
					$q=mysql_query($query) or die();
					$row=mysql_fetch_assoc($q);
					$image=$row['image'];
					if(!empty($image))	 // when no $current image from <form>; but image in database previously			
						{
							$query="update tbl_gallery set title='$title',description='$description',image='$image',date='$date',category='$category',ptype='$priority',status='$status' where id=$id";
							$q=mysql_query($query);
							if($q)
								{
									$_SESSION['msg']="Photo documents updated";
									header("location:../photo.php?page=$page");
								}
							else
								{
									$_SESSION['msg']="Nothing changed.";
									header("location:../photo.php?page=$page");
								}
						}
					else	// when no $current image from <form>; no image in database previously
						{
							$query="DELETE FROM tbl_gallery WHERE id=$id";	// deletes the whole article
							$q=mysql_query($query) or die();
							$_SESSION['msg']="No image related. Article also deleted from tbl_gallery";
							header("location:../photo.php?page=$page");	
							
						} //deletion of photo because no image is there
						
				} //image is not available in <form> end
			} //captcha pass end
	} // update end



/**************************************** end of updating the article **************************************************************************/



?>
