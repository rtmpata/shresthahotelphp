
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Shrestha Hotel Login Form</title>
		<meta name="generator" content="rtmpata@facebook.com" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="../css/bootstrap.css" />
	</head>
<body>
	<!--login modal-->
	<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  	<div class="modal-dialog">
  	<div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center"><i class="glyphicon glyphicon-log-in"></i> Login</h1>
      </div>
      <div class="modal-body">
          <form class="form col-md-12 center-block">
            <div class="form-group">
            	<span class="input-group">
            	<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input type="text" name="username" class="form-control input-lg" placeholder="user name">
             </span>
            </div>
            <div class="form-group">
            	<span class="input-group">
            	<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
              <input type="password" name="pwd" class="form-control input-lg" placeholder="Password">
              	</span>
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-lg btn-block">Sign In</button> 
                 <span class="nav-header">
              		<a href="#" data-toggle="collapse" data-target="#forgot">Forgot Password ?</a>
        				
            		<ul class="list unstyled collapse" id="forgot">
            			<li class="input-group">
            				<h5><i class="glyphicon glyphicon-user"></i> User name</h5>
            			</li>
                		<li class="input-group">
                			<input type="text" class="form-control" placeholder="user name" />
                			<span class="input-group-btn">
                				<span class="nav-header">
              					<a href="#" data-toggle="collapse" data-target="#question"><button class="btn btn-info"><i class="glyphicon glyphicon-user"></i> Go!</button></a>
                				</span>
                			</span>
                		</li>
                			<ul class="list unstyled collapse" id="question">
                				<li class="input-group">
                					<h5><i class="glyphicon glyphicon-flag"></i> What is your Phone Number?</h5>
                				</li>
                				<li class="input-group">
                					<input type="text" class="form-control" placeholder="Answer" />
                					<span class="input-group-btn">
                						<span class="nav-header">
              							<a href="#" data-toggle="collapse" data-target="#answer"><button class="btn btn-info"><i class="glyphicon glyphicon-user"></i> Go!</button></a>
                						</span>
                					</span>
                				</li>
                			</ul>
                			<ul class="list unstyled collapse" id="answer">
                				<li class="input-group">
                					<h5><i class="glyphicon glyphicon-log-in"></i> Enter New Password..</h5>
                				</li>
                				<li class="input-group">
                					<input type="text" class="form-control" placeholder="New Password" />
                					<span class="input-group-addon">
              							<i class="glyphicon glyphicon-pencil"></i>
              						</span>
                				</li>
                				<li class="input-group">
                					<h5>Confirm New Password.</h5>
                				</li>
                				<li class="input-group">
                					<input type="text" class="form-control" placeholder="Retype New Password" />
                					<span class="input-group-btn">
                						<span class="nav-header">
              							<a href="#"><button class="btn btn-info"><i class="glyphicon glyphicon-lock"></i> Submit</button></a>
                						</span>
                					</span>
                				</li>

                			</ul>
                		</ul>
                		</span> <!--forget pasword -->
                   </div>
          </form>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          	<span class="nav-header">
          		<a href="#" data-toggle="collapse" data-target=""><button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Cancel</button></a>
          	</span>
		  </div>	
      </div>
  </div>
  </div>
</div>
	<!-- script references -->
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
</html>