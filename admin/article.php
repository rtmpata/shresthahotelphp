
<?php require_once("connect.php"); ?>
<link href="../css/bootstrap.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="../css/bootstrap-media-lightbox.css" />

<!-- js links -->
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap-media-lightbox.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

	<div class="container">
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th colspan="12" bgcolor="#C8CCD5" >
					<a href="action/add-article.php"><button class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add Article</button></a>
					<a href="index.php"><button class="btn btn-primary"><i class="glyphicon glyphicon-Home"></i> Home</button></a>
					<a href="photo.php?page=1"><button class="btn btn-default"><i class="glyphicon glyphicon-picture"></i> Gallery</button></a>
					<a href="user.php?page=1"><button class="btn btn-default"><i class="glyphicon glyphicon-user"></i> User</button></a>
					<a href="dashboard.php"><button class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i> Booking</button></a>
					<a href="dashboard.php"><button class="btn btn-default"><i class="glyphicon glyphicon-question-sign"></i> About</button></a>
					<!--$lastpage from pagination is sent to addarticle.php-->
				</th>

	<!--- session messages from add-article.php, act-article.php, edit-article.php --->

			<p style="color: orange; text-align: center;">
				<?php
						if(isset($_GET['page']))
						{
							$page=$_GET["page"];
						}
					else
						{
							$page=1;
						}
					if(!empty($_SESSION['msg']))
   						{ 
							echo "<b>".$_SESSION['msg']."</b>";
							session_unset();
						}
				
				?>
			</p>

			</tr>
			<tr>
				<th>S.N</th>
				<th>Title</th>
				<th>Image</th>
				<th>Descriptioj</th>
				<th>Date</th>
				<th>Category</th>
				<th>P.Type</th>
				<th>Status</th>
				<th>Update</th>
			</tr>
			
			
	 <?php
			$tableName="tbl_article";		
			$targetpage = "article.php"; 	
			$limit = 5; 
	
			$query = "SELECT COUNT(*) as num FROM $tableName";
			$total_pages = mysql_fetch_array(mysql_query($query));
			$total_pages = $total_pages['num'];
	
			$stages = 3;
			$page = mysql_real_escape_string($_GET['page']);
			if($page){
				$start = ($page - 1) * $limit; 
			}else{
				$start = 0;	
				}	
	
			// Get page data
			$query1 = "SELECT * FROM $tableName LIMIT $start, $limit";
			$result = mysql_query($query1);
	
			// Initial page num setup
			if ($page == 0){$page = 1;}
			$prev = $page - 1;	
			$next = $page + 1;							
			$lastpage = ceil($total_pages/$limit);		
			$LastPagem1 = $lastpage - 1;					
	
	
			$paginate = '';
			if($lastpage > 1)
			{	
	

	
	
				$paginate .= "<div class='paginate'>";
				// Previous
				if ($page > 1){
					$paginate.= "<a href='$targetpage?page=$prev'>previous</a>";
				}else{
					$paginate.= "<span class='disabled'>previous</span>";	}
			

		
				// Pages	
				if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page){
							$paginate.= "<span class='current'>$counter</span>";
						}else{
							$paginate.= "<a href='$targetpage?page=$counter'>$counter</a>";}					
					}
				}
				elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
				{
					// Beginning only hide later pages
					if($page < 1 + ($stages * 2))		
					{
						for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
						{
							if ($counter == $page){
								$paginate.= "<span class='current'>$counter</span>";
							}else{
								$paginate.= "<a href='$targetpage?page=$counter'>$counter</a>";}					
						}
						$paginate.= "...";
						$paginate.= "<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>";
						$paginate.= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";		
					}
					// Middle hide some front and some back
					elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
					{
						$paginate.= "<a href='$targetpage?page=1'>1</a>";
						$paginate.= "<a href='$tacrgetpage?page=2'>2</a>";
						$paginate.= "...";
						for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
						{
							if ($counter == $page){
								$paginate.= "<span class='current'>$counter</span>";
							}else{
								$paginate.= "<a href='$targetpage?page=$counter'>$counter</a>";}					
						}
						$paginate.= "...";
						$paginate.= "<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>";
						$paginate.= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";		
					}
					// End only hide early pages
					else
					{
						$paginate.= "<a href='$targetpage?page=1'>1</a>";
						$paginate.= "<a href='$targetpage?page=2'>2</a>";
						$paginate.= "...";
						for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page){
								$paginate.= "<span class='current'>$counter</span>";
							}else{
								$paginate.= "<a href='$targetpage?page=$counter'>$counter</a>";}					
						}
					}
				}
					
						// Next
				if ($page < $counter - 1){ 
					$paginate.= "<a href='$targetpage?page=$next'>next</a>";
				}else{
					$paginate.= "<span class='disabled'>next</span>";
					}
			
				$paginate.= "</div>";		
	
	
		}

		 // pagination
		 echo $paginate;
		?>



		<?php 
		 $i=1;

				while($row = mysql_fetch_array($result))
				{
						?>
						<tr height="100px">
							<th><?php echo $i ?></th>
							<th><?php echo substr($row['title'],0,20); ?></th>
							<td><a href="../img/article/<?php echo $row['image']; ?>" class="lightbox" title="<?php echo $row['title']; ?>">
								<img  class="img-rounded" src="../img/article/<?php echo $row['image']; ?>" width="100px" /></a>
							</td>
							<td><?php echo substr($row['description'],0,50); ?></td>
							<td><?php echo $row['date']; ?></td>
							<td><?php echo $row['category']; ?></td>
							<td><?php echo $row['ptype']; ?></td>
							<td><?php echo $row['status']; ?></td>
		
		<!---- edit <button> ===>action/edit-article.php && Delete <button> ===> actin/act-article.php ------------>
			
							<td><a href="action/edit-article.php?eid=<?php echo $row['id']; ?>&page=<?php echo $page; ?>">
									<button class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i></button></a>
								<a href="action/act-article.php?did=<?php echo $row['id']; ?>&page=<?php echo $page; ?>">
									<button onclick="return confirm('Do you want to Delete !!!')" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
								</a>
							</td>
						</tr>
						
				<?php
					$i++;
					}
					?>
		</table>

	</div>
	
<div>	
