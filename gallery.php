<?php
	require_once("require_head.php");
	require_once("require_nav.php");

?>
 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
<meta property="og:image" content="img/bootstrap-social.png" />
<script src="js/jquery.js"></script>
<link rel="stylesheet" href="css/bootstrap-media-lightbox.css" />
<script src="js/bootstrap-media-lightbox.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<body>
<div class="container" style="margin-top: 80px;">
	<ol class="breadcrumb">
			<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li class="active"><span class="glyphicon glyphicon-briefcase"></span> Gallery</li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-sm-8">
				  
    				<h2 style="background-color: #454656; color: white; text-indent: 20px; border-radius: 3px 3px 0px 0px;">Bandipur <small>photos</small></h2>
   					<div class="row">
      				<div class="col-lg-3 col-sm-4 col-xs-6">
      					<a href="img/0141293151898ca8-zee279.jpg" class="lightbox" title="Bandipur">
      						<img class="thumbnail img-responsive" src="img/0141293151898ca8-zee279.jpg">
      					</a>
      				</div>
     				<div class="col-lg-3 col-sm-4 col-xs-6">
      					<a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="Bandipur">
      						<img class="thumbnail img-responsive" src="img/Maraetaibeforesunrise.jpg">
      					</a>
      				</div>
      				<div class="col-lg-3 col-sm-4 col-xs-6">
      					<a href="img/0141293151898ca8-zee279.jpg" class="lightbox">
      						<img class="thumbnail img-responsive" src="img/0141293151898ca8-zee279.jpg">
      					</a>
      				</div>
     				<div class="col-lg-3 col-sm-4 col-xs-6">
      					<a href="img/Maraetaibeforesunrise.jpg" class="lightbox" title="Bandipur">
      						<img class="thumbnail img-responsive" src="img/Maraetaibeforesunrise.jpg">
      					</a>
      				</div>
      			</div>
		
		</article>
		
			<!-- Sidebar -->
			<aside class="col-sm-4" style="padding-left: 5px">

				<div class="widget">
					<h4 style="background-color: #454656; color: white; text-indent: 5px; border-radius: 3px 3px 0px 0px;"><span class="glyphicon glyphicon-glass"></span> Near about</h4>
					<ul class="list-unstyled list-spaces" style="padding-left: 10px;">
						<li><a href=""><span class="glyphicon glyphicon-link"></span>K garne Cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Bandipur cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Gurung Dai ko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>HUriko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span> Aasutosh ko hotel</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
					</ul>
				</div>

			</aside>
			<!-- /Sidebar -->
		

<hr class="divider" />
		<article class="col-sm-12">
					<h1 style="background-color: #454656; color: white; text-indent: 20px; border-radius: 3px 3px 0px 0px;">Events</h1>
				
    				
      				<div class="row">
					<div class="col-xs-3 col-sm-4 col-lg-3" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8 col-lg-9">
						<h4 class="text-success">Fulpaati Puja</h4>
						<p class="text-justified">its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
						 and scrumptious pastries, without missing all the 
						action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outside</p>
						<a href="#"><span class="label label-info">Read more....</span></a>
					</div>
					</div>
					
<hr />
					
     				<div class="row">
					<div class="col-xs-3 col-sm-4 col-lg-3" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8 col-lg-9">
						<h4 class="text-success">Tamu Lhoshar</h4>
						<p class="text-justified">its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
						 and scrumptious pastries, without missing all the 
						action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outside</p>
						<a href="#"><span class="label label-info">Read more....</span></a>
					</div>
				</div>
<hr class="divider" />
   
  </div>
  </article>



			


		</div>
	
</div>
<?php
	require_once("require_foot.php");
?>
	

</body>