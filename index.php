
<?php require_once('require_head.php'); ?>
 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
<meta property="og:image" content="img/bootstrap-social.png" />
<body>
	<?php
    	require_once("require_nav.php");
			require_once("require_slide.php");

    ?>
		<div style="background-color: #191919;; color: white;">
    		<marquee behavior="scroll" scrollamount="3">
            <a href="require_reservation.php"><button class="btn btn-success"> BOOK NOW </button></a></marquee>
   		</div>
<div class="container">
<div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-success"><strong>Welcome to Bandipur</strong></h1>
            </div>

            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-success"><i class="fa fa-fw fa-check-square-o"></i><strong> Shrestha Hotel</strong></h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur
                            in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?
                        </p>
                        <a href="#" class="btn btn-success">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-success"><i class="fa fa-fw fa-gift"></i><strong> Our Speciality</strong> </h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur
                            in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?
                        </p>
                        <a href="#" class="btn btn-success">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-success"><i class="fa fa-fw fa-compass"></i><strong> Explore Bandipur</strong></h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur
                            in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?
                        </p>
                        <a href="#" class="btn btn-success">Learn More</a>
                    </div>
                </div>
            </div>
          </div>
            <div class="row jumbotron">
                <div class="col-md-8">
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias,
                      expedita, saepe, vero rerum deleniti beatae veniam harum neque
                      nemo praesentium cum alias asperiores commodi.
                    </p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-success btn-block" href="#">Let's Visit Bandipur</a>
                </div>
            </div>
  </div>




		<div>
			<h3 class="text-success" style="padding-left: 10px;"><strong>Our Location</strong></h3>
	    	<div class="google-maps">
          <iframe
          <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3524.854061314409!2d84.40251141458867!3d27.937118882697295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399504f1adc07b69%3A0xb91176b94ef4c9b8!2sShrestha+Hotel+and+Lodge!5e0!3m2!1sen!2snp!4v1470296326017"
           zoom="20" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

    		</div>
		</div>







<!---footer -->

	<?php require_once("require_foot.php"); ?>
</body>
