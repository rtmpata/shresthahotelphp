<?php
	require_once("require_head.php");
	require_once("require_nav.php");

?>
 <link href="css/font-awesome.css" rel="stylesheet">
 <link href="css/bootstrap-social.css" rel="stylesheet"/>
<meta property="og:image" content="img/bootstrap-social.png" />
<body>
<div class="container" style="margin-top: 80px;">
	<ol class="breadcrumb">
			<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			<li class="active"><span class="glyphicon glyphicon-cutlery"></span> Dining</li>
		</ol>

		<div class="row">
			
			<!-- Article main content -->
			<article class="col-sm-8">
					<h1 style="background-color: #454656; color: white; text-indent: 5px; border-radius: 3px 3px 0px 0px;text-indent: 20px;">Dining and Entertainment</h1>

					<!-----------  main dining hall ------------>
				<div class="row">
					<div class="col-xs-3 col-sm-4" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8">
						<h4 class="text-success">Main Dining Hall</h4>
						<p class="text-justified">its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
						 and scrumptious pastries, without missing all the 
						action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outside</p>
					</div>
				</div>
				
				<hr class="divider" />
				
					<!-------------- terrace tea lounge ------------>
				<div class="row">
					<div class="col-xs-3 col-sm-4" style="border-right: 2px solid orange;">
						<img src="img/Maraetaibeforesunrise.jpg" alt="" class="img-rounded img-thumnail pull-left img-responsive" max-width="150px" >
					</div>
					<div class="col-xs-9 col-sm-8">
						<h4 class="text-success">Terrace Tea Lounge</h4>
						<p class="text-justified">its feel homeAn action junction, catch up on your mails or surf the internet indulging over a cup of tea/coffee
						 and scrumptious pastries, without missing all the 
						action in the  lobby area  with the sound of soft running water in the landscaped Japanese garden outside</p>
					</div>
				</div>
				
		<hr class="divider" />
				
		</article>
			<!-- /Article -->
			
			<!-- Sidebar -->
			<aside class="col-sm-4">

				<div class="widget">
					<h4 style="background-color: #454656; color: white; text-indent: 5px; border-radius: 3px 3px 0px 0px;"><span class="glyphicon glyphicon-glass"></span> Near about</h4>
					<ul class="list-unstyled list-spaces" style="padding-left: 10px;">
						<li><a href=""><span class="glyphicon glyphicon-link"></span>K garne Cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Bandipur cafe</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>Gurung Dai ko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span>HUriko restaurent</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
						<li><a href=""><span class="glyphicon glyphicon-link"></span> Aasutosh ko hotel</a><br>
							<p style="text-indent: 15px;"><span class="small text-muted">it is a cave</span></p></li>
					</ul>
				</div>

			</aside>
			<!-- /Sidebar -->

		</div>
	
</div>

	<?php
		require_once("require_foot.php");
	?>
	
</body>